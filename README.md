<h1 >markdown：</h1>
<div>[TOC]</div>
<p>&nbsp;</p>
<!--downmarkdown也是我刚上手不久的一款笔记软件-->
<p>对于markdown，是一个是个轻度排版，😄重度内容的软件，关于typora它也是特别简介的。</p>
<p>从我自己的角度上面，比较喜欢的OneNote，markdown，GitHub，vim，同样的，也有很多丰富的插件。</p>
<p>对于代码来说，无疑markdown是最好的选择之一，可以看下<strong>C++</strong>的代码模板：</p>
<pre><code class='language-c++' lang='c++'>#include&lt;iostream&gt;`

`uring namespace std;`

`int main()`

`{`

​		`cout&lt;&lt;&quot;hello word&quot;&lt;&lt;endl;`

​		`return 0;`

`}
</code></pre>
<p>mark down支持六种标题，分别对应的不同的样式。</p>
<hr />
<h3 ><em>怎么</em>使用？</h3>
<p>如果不算<strong>扩展</strong>，Markdown的语法绝对<strong>简单</strong>到让你爱不释手。</p>
<p>Markdown语法主要分为如下几大部分： <strong>标题</strong>，<strong>段落</strong>，<strong>区块引用</strong>，<strong>代码区块</strong>，<strong>强调</strong>，<strong>列表</strong>，<strong>分割线</strong>，<strong>链接</strong>，<strong>图片</strong>，<strong>反斜杠 <code>\</code></strong>，<strong>符号&#39;`&#39;</strong>。</p>
<h4 >1.标题</h4>
<p>两种形式：
1）使用<code>=</code>和<code>-</code>标记一级和二级标题。</p>
<blockquote><p>一级标题
<code>=========</code>
二级标题
<code>---------</code></p>
</blockquote>
<p>效果：</p>
<blockquote><h1 >一级标题</h1>
<h2 >二级标题</h2>
</blockquote>
<p>2）使用<code>#</code>，可表示1-6级标题。</p>
<blockquote><p># 一级标题
## 二级标题
### 三级标题
#### 四级标题
##### 五级标题
###### 六级标题</p>
</blockquote>
<p>效果：</p>
<blockquote><h1 >一级标题</h1>
<h2 >二级标题</h2>
<h3 >三级标题</h3>
<h4 >四级标题</h4>
<h5 >五级标题</h5>
<h6 >六级标题</h6>
</blockquote>
<h4 >2 段落</h4>
<p>段落的前后要有空行，所谓的空行是指没有文字内容。若想在段内强制换行的方式是使用<strong>两个以上</strong>空格加上回车（引用中换行省略回车）。</p>
<h4 >3 区块引用</h4>
<p>在段落的每行或者只在第一行使用符号<code>&gt;</code>,还可使用多个嵌套引用，如：</p>
<blockquote><p>&gt; 区块引用
&gt;&gt; 嵌套引用</p>
</blockquote>
<p>效果：</p>
<blockquote><p>区块引用</p>
<blockquote><p>嵌套引用</p>
</blockquote>
</blockquote>
<h4 >4 代码区块</h4>
<p>代码区块的建立是在每行加上4个空格或者一个制表符（如同写代码一样）。如
普通段落：</p>
<p>void main()
{
	printf(&quot;Hello, Markdown.&quot;);
}</p>
<p>代码区块：</p>
<pre><code class='language-c' lang='c'>void main()
{
    printf(&quot;Hello, Markdown.&quot;);
}
</code></pre>
<p><strong>注意</strong>:需要和普通段落之间存在空行。</p>
<h4 >5 强调</h4>
<p>在强调内容两侧分别加上<code>*</code>或者<code>_</code>，如：</p>
<blockquote><p><em>斜体</em>，<em>斜体</em>
<strong>粗体</strong>，<strong>粗体</strong></p>
</blockquote>
<p>效果：</p>
<blockquote><p><em>斜体</em>，<em>斜体</em>
<strong>粗体</strong>，<strong>粗体</strong></p>
</blockquote>
<h4 >6 列表</h4>
<p>使用<code>·</code>、<code>+</code>、或<code>-</code>标记无序列表，如：</p>
<blockquote><p>-（+<em>） 第一项 -（+</em>） 第二项 - （+*）第三项</p>
</blockquote>
<p><strong>注意</strong>：标记后面最少有一个<em>空格</em>或<em>制表符</em>。若不在引用区块中，必须和前方段落之间存在空行。</p>
<p>效果：</p>
<blockquote><ul>
<li>第一项</li>
<li>第二项</li>
<li>第三项</li>

</ul>
</blockquote>
<p>有序列表的标记方式是将上述的符号换成数字,并辅以<code>.</code>，如：</p>
<blockquote><p>1 . 第一项
2 . 第二项
3 . 第三项</p>
</blockquote>
<p>效果：</p>
<blockquote><ol>
<li>第一项</li>
<li>第二项</li>
<li>第三项</li>

</ol>
</blockquote>
<h4 >7 分割线</h4>
<p>分割线最常使用就是三个或以上<code>*</code>，还可以使用<code>-</code>和<code>_</code>。</p>
<h4 >8 链接</h4>
<p>链接可以由两种形式生成：<strong>行内式</strong>和<strong>参考式</strong>。
<strong>行内式</strong>：</p>
<blockquote><p><a href='https://github.com/3293172751' title='Markdown'>smile的Markdown库</a>。</p>
</blockquote>
<p>效果：</p>
<blockquote><p><a href='https://github.com/3293172751'>smile的Markdown库</a>。</p>
</blockquote>
<p><strong>参考式</strong>：</p>
<blockquote><p><a href='https://github.com/3293172751' target='_blank' title='Markdown'>smile的Markdown库1</a>
<a href='https://github.com/329172751' target='_blank' title='Markdown'>smile的Markdown库2</a></p>


</blockquote>
<p>效果：</p>
<blockquote><p><a href='https://github.com/3293172751'>smile的Markdown库1</a>
<a href='https://github.com/3293172751'>smile的Markdown库2</a></p>
</blockquote>
<p><strong>注意</strong>：上述的<code>[1]:https:://github.com/3293172751 &quot;Markdown&quot;</code>不出现在区块中。</p>
<h4 >9 图片</h4>
<p>添加图片的形式和链接相似，只需在链接的基础上前方加一个<code>！</code>。</p>
<h4 >10 反斜杠<code>\</code></h4>
<p>相当于<strong>反转义</strong>作用。使符号成为普通符号。</p>
<p>&nbsp;</p>
<h4 >11 符号&#39;`&#39;</h4>
<p>起到标记作用。如：</p>
<blockquote><pre><code>ctrl+a
</code></pre>
</blockquote>
<p>效果：</p>
<blockquote><pre><code>ctrl+a
</code></pre>
</blockquote>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h4 >尝试一下</h4>
<ul>
<li><strong>Chrome</strong>下的插件诸如<code>stackedit</code>与<code>markdown-here</code>等非常方便，也不用担心平台受限。</li>
<li><strong>在线</strong>的dillinger.io评价也不错</li>
<li><strong>Windowns</strong>下的MarkdownPad也用过，不过免费版的体验不是很好。</li>
<li><strong>Mac</strong>下的Mou是国人贡献的，口碑很好。</li>
<li><strong>Linux</strong>下的ReText不错。</li>

</ul>
<p><strong>当然，最终境界永远都是笔下是语法，心中格式化 :)。</strong></p>
<hr />
<p><strong>注意</strong>：不同的Markdown解释器或工具对相应语法（扩展语法）的解释效果不尽相同，具体可参见工具的使用说明。 虽然有人想出面搞一个所谓的标准化的Markdown，[没想到还惹怒了健在的创始人John Gruber] (<a href='http://blog.codinghorror.com/standard-markdown-is-now-common-markdown/' target='_blank' class='url'>http://blog.codinghorror.com/standard-markdown-is-now-common-markdown/</a> )。</p>
<hr />
<p>以上基本是所有traditonal markdown的语法。</p>
<p>&nbsp;</p>
<h3 >其它：</h3>
<p>列表的使用(非traditonal markdown)：</p>
<p>用<code>|</code>表示表格纵向边界，表头和表内容用<code>-</code>隔开，并可用<code>:</code>进行对齐设置，两边都有<code>:</code>则表示居中，若不加<code>:</code>则默认左对齐。</p>
<figure><table>
<thead>
<tr><th>代码库</th><th>链接</th></tr></thead>
<tbody><tr><td>MarkDown</td><td><a href='https://github.com/3293172751' target='_blank' class='url'>https://github.com/3293172751</a></td></tr><tr><td>MarkDownCopy</td><td><a href='https://github.com/3293172751' target='_blank' class='url'>https://github.com/3293172751</a></td></tr></tbody>
</table></figure>
<p>关于其它扩展语法可参见具体工具的使用说明。</p>
<p>&nbsp;</p>
<h3 >表情：</h3>
<p>在window或者Linux又或者是max同样有一个让人惊喜的功能，简单试一下</p>
<p><code>：输入你要输入的英文或者数字</code></p>
<p>:smile</p>
<p>或者用快捷键<strong>win + 。</strong>😊这是Windows自带的表情标签</p>
<p>&nbsp;</p>
<h3 >markdown插入图片：</h3>
<p>markdown插入图片是一个很特别的功能，并不仅仅是你复制粘贴所得到的那么简明。</p>
<p>它复杂的功能带来了更好的体验。</p>
<p><strong>在你引用本地图片的时候，如果将图片删除后，那么你的图片也会消失</strong>，这样会增加很多的维护成本。</p>
<p>如何解决😦:</p>
<ul>
<li><p>你可以选择网上的图片路径，对路径的地址进行拷贝，在粘贴为图像路径，基本上图片不会失效。</p>
</li>
<li><p>图床思想：</p>
<ul>
<li>PicGo   --适用于windows</li>

</ul>
</li>

</ul>
<p>&nbsp;</p>
<h6 >图片的排版：</h6>
<p>		使用HTML对图片基本操作</p>
<p><img src="https://i.loli.net/2021/11/21/XjbE3uvHoTCFaUl.jpg" referrerpolicy="no-referrer"></p>
<hr />
<h3 >同样要补充的是：</h3>
<p>		在你使用GitHub做笔记，上传项目，或者是做个人博客的时候，GitHub一般选择的是图床，即你不会将GitHub上面的图片。</p>
<p>而是一般选择将你的图片上传到一个服务器或者是上传到一个平台，使用<code>特有的连接</code>去访问。</p>
<p>关于GitHub的Pic Go仓库是一个开源的，可以免费使用的仓库。</p>
<p>PicGo-Setup下载之后即可直接进行安装，使用，你就不用担心自己的GitHub或者你的博客卡顿问题了</p>
<p><a href='https://github.com/Molunerfinn/PicGo/releases/download/v2.3.0/PicGo-Setup-2.3.0-x64.exe'>这个是GitHub上面的下载地址：https://github.com/Molunerfinn/PicGo/releases/download/v2.3.0/PicGo-Setup-2.3.0-x64.exe</a></p>
<p>&nbsp;</p>
<p>你可以使用你的GitHub用来存放图片，但是我不建议这么做，GitHub在国内的下载速度很慢，可能会出现图片加载速度缓慢的情况😂</p>
